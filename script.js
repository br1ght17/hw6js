// 1

let product = {
    name : "iPhone",
    price: "2700$",
    discount: "11%",
    fullPrice (){return parseInt(this.price) * (100 - parseInt(this.discount)) / 100}
}

console.log(`Повна ціна на цей продукт складає $${product.fullPrice()}`)

// 2

let name = prompt("What's your name")
let age = +prompt("How old are you")

let data = {
    name,
    age
}

function greet(x){
    if(x.age === 11 && x.age === 12 && x.age === 13 && x.age === 14){
        return`Привіт, мені ${x.age} років`
    }
    else if(x.age % 10 === 1 ){
        return `Привіт, мені ${x.age} рік`
    }
    else if(x.age % 10 === 2 && x.age % 10 === 3 && x.age % 10 === 4){
        return `Привіт, мені ${x.age} роки`
    }
    else{
        return`Привіт, мені ${x.age} років`
    }
}

alert(greet(data))

// 3 

function cloneObj(x){
    let clonnedObj = {}
    for(let i in x){
        clonnedObj[i] = x[i]
    }
    return clonnedObj
}

let product2 = cloneObj(product)
console.log(product2)   